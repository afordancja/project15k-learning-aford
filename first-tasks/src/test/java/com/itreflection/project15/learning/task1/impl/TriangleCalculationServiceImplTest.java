package com.itreflection.project15.learning.task1.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.slf4j.LoggerFactory.getLogger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

import com.itreflection.project15.learning.task1.api.TriangleParameterException;

@RunWith(MockitoJUnitRunner.class)
public class TriangleCalculationServiceImplTest {

    private final Logger logger = getLogger(TriangleCalculationServiceImplTest.class);

    @InjectMocks
    private TriangleCalculationServiceImpl triangleCalculationService;

    @Test
    public void shouldCalculatePerimeterForCorrectParameters() throws Exception {
        //given
        int a = 1, b = 1, c = 1;

        //when
        double result = triangleCalculationService.calculatePerimeter(a, b, c);

        //then
        assertThat(result).isEqualTo(3);
    }

    @Test
    public void shouldThrowExceptionWhenNegativeValues() throws Exception {
        //given
        int a = -1, b = -2, c = -3;

        //when
        try {
            double result = triangleCalculationService.calculatePerimeter(a, b, c);
            fail("For given parameters exception should be thrown");
        } catch (Exception e) {
            //then
            assertThat(e).isExactlyInstanceOf(TriangleParameterException.class);
            assertThat(e.getMessage()).isEqualTo("Parameter a is not greater than 0");
        }
    }

    @Test
    public void shouldThrowExceptionWhenTraiangleImpossible() throws Exception {
        //given
        int a = 1, b = 2, c = 3;

        //when
        try {
            double result = triangleCalculationService.calculatePerimeter(a, b, c);
            fail("For given parameters exception should be thrown");
        } catch (Exception e) {
            //then
            assertThat(e).isExactlyInstanceOf(TriangleParameterException.class);
            assertThat(e.getMessage()).isEqualTo("Its impossible to create a triangle");
        }
    }

    @Test
    public void shouldThrowExceptionWhenANotGreaterThan0() throws Exception {
        //given
        int a = 0, b = 0, c = 0;

        //when
        try {
            double result = triangleCalculationService.calculatePerimeter(a, b, c);
            fail("For given parameters exception should be thrown");
        } catch (Exception e) {

            //then
            assertThat(e).isExactlyInstanceOf(TriangleParameterException.class);
            assertThat(e.getMessage()).isEqualTo("Parameter a is not greater than 0");
        }
    }

}
