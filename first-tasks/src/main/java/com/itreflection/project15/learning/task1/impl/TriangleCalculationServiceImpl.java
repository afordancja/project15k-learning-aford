package com.itreflection.project15.learning.task1.impl;

import com.itreflection.project15.learning.task1.api.TriangleCalculationService;
import com.itreflection.project15.learning.task1.api.TriangleParameterException;

/**
 * Just for fun
 * @author Afordancja
 */
public class TriangleCalculationServiceImpl implements TriangleCalculationService {

    @Override
    public double calculatePerimeter(int a, int b, int c) {
        if (a <= 0 || b <= 0 || c <= 0) {
            throw new TriangleParameterException("Parameter a is not greater than 0");
        }
        if (a + b <= c || a + c <= b || c + b <= a) {
            throw new TriangleParameterException("Its impossible to create a triangle");
        }
        return a + b + c;
    }
}
