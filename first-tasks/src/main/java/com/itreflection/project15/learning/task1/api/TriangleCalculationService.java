package com.itreflection.project15.learning.task1.api;

public interface TriangleCalculationService {

  double calculatePerimeter(int a, int b, int c);
}
